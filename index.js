require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;
const cors = require('cors');
const morgan = require('morgan');

const authRoute = require('./routes/auth');
const trucksRoute = require('./routes/trucks');
const loadsRoute = require('./routes/loads');
const usersRoute = require('./routes/users');

app.use(morgan(':method :url :status :body'));
morgan.token('body', (req) => JSON.stringify(req.body));


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());
app.use('/api/auth', authRoute);
app.use('/api/trucks', trucksRoute);
app.use('/api/loads', loadsRoute);
app.use('/api/users', usersRoute);
app.use('*', (req, res) => {
  morgan.token('body', (req) => JSON.stringify({message: 'Not Found'}));
  return res.status(404).send({message: 'Not Found'});
});

mongoose.connect(process.env.MONGI_URI || 'mongodb://localhost/users', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on('error', (err) => {
  console.log(err);
});
db.once('open', () => {
  console.log('db opened successfully');
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
