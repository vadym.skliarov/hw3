const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const controller = require('../controllers/loads.conroller');
const utils = require('../utils/utils');

router.get('/', utils.authenticateToken, controller.getLoads);

router.get('/active', utils.authenticateToken, controller.getLoadActive);

router.post('/', utils.authenticateToken, controller.addLoads);

router.put('/:id', utils.authenticateToken, controller.modifyLoads);

router.get('/:id', utils.authenticateToken, controller.getLoad);

router.delete('/:id', utils.authenticateToken, controller.deleteLoads);

router.patch('/active/state',
    utils.authenticateToken,
    controller.changeStateLoad,
);

router.post('/:id/post', utils.authenticateToken, controller.findDeliver);

router.get('/:id/shipping_info',
    utils.authenticateToken,
    controller.getLoadShipping,
);


module.exports = router;
