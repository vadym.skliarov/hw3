const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const controller = require('../controllers/trucks.conroller');
const utils = require('../utils/utils');

router.get('/', utils.authenticateToken, controller.getTrucks);

router.post('/', utils.authenticateToken, controller.addTruck);

router.get('/:id', utils.authenticateToken, controller.getTruck);

router.put('/:id', utils.authenticateToken, controller.modifyTruck);

router.delete('/:id', utils.authenticateToken, controller.deleteTruck);

router.post('/:id/assign', utils.authenticateToken, controller.assignTrack);


module.exports = router;
