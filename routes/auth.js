const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const controller = require('../controllers/auth.controller');

router.post('/login', controller.login);

router.post('/register', controller.register);

router.post('/forgot_password', controller.forgot_password);


module.exports = router;
