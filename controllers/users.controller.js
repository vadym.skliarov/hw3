const User = require('../models/user');
const bcrypt = require('bcrypt');
const morgan = require('morgan');
const rounds = 10;

exports.getUser= async (req, res) => {
  try {
    const userJWT = req.user;
    const id = userJWT.id;
    console.log(id);
    const user = await User.findOne({id});
    const userWithoutPass = {
      _id: user._id,
      email: user.email,
      role: user.role,
      created_date: user.created_date,
    };
    morgan.token('body', (req) => JSON.stringify({user: userWithoutPass}));
    return res.status(200).send({user: userWithoutPass});
  } catch (error) {
    console.log(error);
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};

exports.deleteUser = async (req, res) => {
  try {
    const userJWT = req.user;
    const id = userJWT.id;
    const user = await User.findOne({id});

    await user.remove();
    morgan.token('body', (req) => JSON.stringify({
      message: 'Profile deleted successfully',
    }));
    return res.status(200).send({message: 'Profile deleted successfully'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};

exports.changePassword = async (req, res) => {
  try {
    const {newPassword, oldPassword} = req.body;
    const userJWT = req.user;
    const id = userJWT.id;
    const user = await User.findOne({id});

    if (!oldPassword || typeof oldPassword !== 'string') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Invalid password'});
      });
      return res.status(400).send({message: 'Invalid password'});
    }
    if (oldPassword.length < 5) {
      morgan.token('body', (req) => {
        return JSON.stringify({
          message: 'Password too small. Should be atleast 6 characters',
        });
      });
      return res.status(400).send({
        message: 'Password too small. Should be atleast 6 characters',
      });
    }

    if (await bcrypt.compare(oldPassword, user.password)) {
      hashedPassword = await bcrypt.hash(newPassword, rounds);
      await User.updateOne(
          {id},
          {
            $set: {password: hashedPassword},
          },
      );
      morgan.token('body', (req) => JSON.stringify({
        message: 'Password changed successfully',
      }));
      return res.status(200).send({message: 'Password changed successfully'});
    }
    morgan.token('body', (req) => JSON.stringify({message: 'Wronge password'}));
    return res.status(400).send({message: 'Wronge password'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
