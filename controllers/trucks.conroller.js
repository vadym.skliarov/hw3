const Trucks = require('../models/truck');
const morgan = require('morgan');

exports.addTruck = async (req, res) => {
  try {
    const user = req.user;
    const type = req.body.type;
    if (user.role.toLowerCase() !== 'driver') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'abailable only for Driver'});
      });
      return res.status(400).send({message: 'abailable only for Driver'});
    }
    if (!type.trim()) {
      morgan.token('body', (req) => JSON.stringify({message: 'Type is empty'}));
      return res.status(400).send({message: 'Type is empty'});
    }
    const truck = await Trucks.findOne({assigned_to: user.id});
    if (truck) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Driver can have only one truck'});
      });
      return res.status(400).send({message: 'Driver can have only one truck'});
    }
    const createdDate = new Date().toJSON();
    await Trucks.create({
      created_by: user.id,
      assigned_to: null,
      type,
      status: 'IS',
      created_date: createdDate,
    });
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Truck created successfully'});
    });
    return res.status(200).send({message: 'Truck created successfully'});
  } catch (error) {
    console.log(error);
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.getTruck = async (req, res) => {
  try {
    const id = req.params.id;
    const user = req.user;
    if (user.role.toLowerCase() !== 'driver') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'abailable only for Driver'});
      });
      return res.status(400).send({message: 'abailable only for Driver'});
    }
    const truck = await Trucks.findOne({_id: id, created_by: user.id});
    if (!truck) {
      morgan.token('body', (req) => JSON.stringify({message: 'Not found'}));
      return res.status(400).send({message: 'Not found'});
    }
    morgan.token('body', (req) => JSON.stringify({truck}));
    return res.status(200).send({truck});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.getTrucks = async (req, res) => {
  try {
    const user = req.user;
    if (user.role.toLowerCase() !== 'driver') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'abailable only for Driver'});
      });
      return res.status(400).send({message: 'abailable only for Driver'});
    }
    const trucks = (await Trucks.find({created_by: user.id}));
    morgan.token('body', (req) => JSON.stringify({trucks}));
    return res.status(200).send({trucks});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.modifyTruck = async (req, res) => {
  try {
    const user = req.user;
    const id = req.params.id;
    const type = req.body.type;
    if (user.role.toLowerCase() !== 'driver') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'abailable only for Driver'});
      });
      return res.status(400).send({message: 'abailable only for Driver'});
    }
    const truck = await Trucks.findOne({_id: id});
    if (!truck) {
      morgan.token('body', (req) => JSON.stringify({message: 'Not found'}));
      return res.status(400).send({message: 'Not found'});
    }
    if (truck.assigned_to === user.id) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: `You can't change your truck`});
      });
      return res.status(400).send({message: `You can't change your truck`});
    }
    if (truck.status === 'OL') {
      morgan.token('body', (req) => {
        return JSON.stringify({
          message: `You can't change truck while hi is on a load`,
        });
      });
      return res.status(400).send({
        message: `You can't change truck while hi is on a load`,
      });
    }
    await Trucks.updateOne({_id: id}, {type});
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Truck details changed successfully'});
    });
    return res.status(200).send({
      message: 'Truck details changed successfully',
    });
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.deleteTruck = async (req, res) => {
  try {
    const user = req.user;
    const id = req.params.id;
    if (user.role.toLowerCase() !== 'driver') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'abailable only for Driver'});
      });
      return res.status(400).send({message: 'abailable only for Driver'});
    }
    const truck = await Trucks.findOne({_id: id});
    if (!truck) {
      morgan.token('body', (req) => JSON.stringify({message: 'Not found'}));
      return res.status(400).send({message: 'Not found'});
    }
    if (truck.assigned_to === user.id) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: `You can't change your truck`});
      });
      return res.status(400).send({message: `You can't change your truck`});
    }
    if (truck.status === 'OL') {
      morgan.token('body', (req) => {
        return JSON.stringify({
          message: `You can't change truck while hi is on a load`,
        });
      });
      return res.status(400).send({
        message: `You can't change truck while hi is on a load`,
      });
    }
    await truck.remove();
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Truck deleted successfully'});
    });
    return res.status(200).send({
      message: 'Truck deleted successfully',
    });
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.assignTrack = async (req, res) => {
  try {
    const user = req.user;
    const id = req.params.id;
    if (user.role.toLowerCase() !== 'driver') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'abailable only for Driver'});
      });
      return res.status(400).send({message: 'abailable only for Driver'});
    }
    const truck = await Trucks.findOne({assigned_to: user.id});
    if (truck) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Driver can have only one truck'});
      });
      return res.status(400).send({message: 'Driver can have only one truck'});
    }
    await Trucks.updateOne({_id: id}, {assigned_to: user.id});
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Truck assigned successfully'});
    });
    return res.status(200).send({
      message: 'Truck assigned successfully',
    });
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
