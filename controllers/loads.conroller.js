/* eslint-disable no-tabs */
const Loads = require('../models/load');
const Trucks = require('../models/truck');
const morgan = require('morgan');
/*
SPRINTER = 300x250x170 1700
SMALL STRAIGHT = 500x250x170 2500
LARGE STRAIGHT = 700x350x270 4000
*/
const typeCars = [
  {
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
    type: 'SPRINTER',
  },
  {
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
    type: 'SMALL STRAIGHT',
  },
  {
    width: 700,
    length: 350,
    height: 270,
    payload: 4000,
    type: 'LARGE STRAIGHT',
  },
];

/**
 * Find type weight Car.
 * @param {object} load Load.
 * @return {string} name type.
 */
function findCar(load) {
  const payload = load.payload;
  const width = load.dimensions.width;
  const length = load.dimensions.length;
  const height = load.dimensions.height;
  const result = [];
  for (const typeCar of typeCars) {
    if (typeCar.payload >= payload &&
			typeCar.width >= width &&
			typeCar.length >= length &&
			typeCar.height >= height) {
      result.push(typeCar.type);
    }
  }
  return result;
}

exports.addLoads = async (req, res) => {
  try {
    const user = req.user;
    const load = req.body;
    if (user.role.toLowerCase() !== 'shipper') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'available only for Shipper'});
      });
      return res.status(400).send({message: 'available only for Shipper'});
    }
    const createdDate = new Date().toJSON();
    await Loads.create({
      ...load,
      created_by: user.id,
      assigned_to: null,
      status: 'NEW',
      state: 'En route to Pick Up',
      created_date: createdDate,
    });
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Load created successfully'});
    });
    return res.status(200).send({message: 'Load created successfully'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.modifyLoads = async (req, res) => {
  try {
    const user = req.user;
    const id = req.params.id;
    const newLoad = req.body;
    if (user.role.toLowerCase() !== 'shipper') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'available only for Shipper'});
      });
      return res.status(400).send({message: 'available only for Shipper'});
    }
    const load = await Loads.findOne({_id: id, status: 'NEW'});
    if (!load) {
      morgan.token('body', (req) => {
        return JSON.stringify({
          message: 'Not found load with this id or status NEW',
        });
      });
      return res.status(400).send({
        message: 'Not found load with this id or status NEW',
      });
    }
    const createdDate = new Date().toJSON();
    const log = {
      time: createdDate,
      message: `Load updated by ${user.id}`,
    };
    await Loads.updateOne({_id: id}, {...newLoad, $push: {logs: [log]}});
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Load details changed successfully'});
    });
    return res.status(200).send({
      message: 'Load details changed successfully',
    });
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.deleteLoads = async (req, res) => {
  try {
    const user = req.user;
    const id = req.params.id;
    if (user.role.toLowerCase() !== 'shipper') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'available only for Shipper'});
      });
      return res.status(400).send({message: 'available only for Shipper'});
    }
    const load = await Loads.findOne({_id: id, status: 'NEW'});
    if (!load) {
      morgan.token('body', (req) => {
        return JSON.stringify({
          message: 'Not found load with this id or status NEW',
        });
      });
      return res.status(400).send({
        message: 'Not found load with this id or status NEW',
      });
    }
    await load.remove();
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Load deleted successfully'});
    });
    return res.status(200).send({
      message: 'Load deleted successfully',
    });
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.getLoads = async (req, res) => {
  try {
    const user = req.user;
    const status = req.query.status ?? 'NEW';
    const limit = req.query.limit ?? 0;
    const offset = req.query.offset ?? 0;
    let loads = [];
    if (user.role === 'SHIPPER') {
      loads = await Loads.find(
          {status, created_by: user.id})
          .skip(offset)
          .limit(limit);
    } else {
      loads = await Loads.find(
          {status, assigned_to: user.id})
          .skip(offset)
          .limit(limit);
    }
    if (loads.length) {
      morgan.token('body', (req) => JSON.stringify({loads}));
      return res.status(200).send(
          {loads});
    } else {
      morgan.token('body', (req) => JSON.stringify({message: 'No Loads'}));
      return res.status(400).send({message: 'No Loads'});
    }
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'No Loads'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.getLoad = async (req, res) => {
  try {
    const user = req.user;
    const id = req.params.id;
    let load = '';
    if (user.role.toLowerCase() === 'driver') {
      load = await Loads.findOne({assigned_to: user.id, _id: id});
    } else {
      load = await Loads.findOne({_id: id});
    }
    if (!load) {
      morgan.token('body', (req) => JSON.stringify({message: 'Not found'}));
      return res.status(400).send({message: 'Not found'});
    }
    morgan.token('body', (req) => JSON.stringify({load}));
    return res.status(200).send({load});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.findDeliver = async (req, res) => {
  try {
    const id = req.params.id;
    const user = req.user;
    const createdDate = new Date().toJSON();
    if (user.role.toLowerCase() !== 'shipper') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'available only for Shipper'});
      });
      return res.status(400).send({message: 'available only for Shipper'});
    }
    const load = await Loads.findOne({_id: id});
    if (!load) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Not found Load'});
      });
      return res.status(400).send({message: 'Not found Load'});
    }
    const types = findCar(load);
    const truck = await Trucks.findOne({
      'status': 'IS',
      'type': {
        $in: types,
      },
      'assigned_to': {
        $ne: null,
      },
    });
    if (!truck) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Not found Truck'});
      });
      /* Create log */
      const log = {
        time: createdDate,
        message: `Not Found Driver`,
      };
      await Loads.updateOne({_id: id}, {
        $push: {logs: [log]},
      } );
      return res.status(400).send({message: 'Not found Truck'});
    }

    /* Create log, update load and Truck*/
    const log = {
      time: createdDate,
      message: `Load assigned to driver with id ${truck.assigned_to}`,
    };
    await Loads.updateOne({_id: id}, {
      assigned_to: truck.assigned_to,
      status: 'ASSIGNED',
      $push: {logs: [log]},
    } );
    await Trucks.updateOne({_id: truck._id}, {
      status: 'OL',
    });

    morgan.token('body', (req) => {
      return JSON.stringify({
        message: 'Load posted successfully', driver_found: true,
      });
    });
    return res.status(200).send({
      message: 'Load posted successfully', driver_found: true,
    });
  } catch (error) {
    console.log(error);
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.getLoadActive = async (req, res) => {
  try {
    const user = req.user;
    if (user.role.toLowerCase() !== 'driver') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'abailable only for Driver'});
      });
      return res.status(400).send({message: 'abailable only for Driver'});
    }
    const load = await Loads.findOne({assigned_to: user.id});
    if (!load) {
      morgan.token('body', (req) => JSON.stringify({message: 'Not found'}));
      return res.status(400).send({message: 'Not found'});
    }
    morgan.token('body', (req) => JSON.stringify({load}));
    return res.status(200).send({load});
  } catch (error) {
    console.log('7');
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.changeStateLoad = async (req, res) => {
  try {
    states = [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to Delivery',
      'Arrived to Delivery',
    ];
    const user = req.user;
    if (user.role.toLowerCase() !== 'driver') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'available only for Driver'});
      });
      return res.status(400).send({message: 'available only for Driver'});
    }
    console.log(user.id);
    const load = await Loads.findOne({assigned_to: user.id});
    if (!load) {
      morgan.token('body', (req) => {
        return JSON.stringify({
          message: 'Not found load with this id or status NEW',
        });
      });
      return res.status(400).send({
        message: 'Not found load with this id or status NEW',
      });
    }
    const newState = states.findIndex((x) => x === load.state);
    if (states[newState + 1] === 'Arrived to Delivery') {
      await Loads.updateOne(
          {assigned_to: user.id},
          {status: 'SHIPPED'},
      );
      await Trucks.updateOne(
          {assigned_to: user.id},
          {status: 'IS'},
      );
    }
    await Loads.updateOne(
        {assigned_to: user.id},
        {state: states[newState + 1] ?? states[0]},
    );
    morgan.token('body', (req) => {
      return JSON.stringify({
        message: `Load state changed to '${states[newState + 1] ?? states[0]}'`,
      });
    });
    return res.status(200).send({
      message: `Load state changed to '${states[newState + 1] ?? states[0]}'`,
    });
  } catch (error) {
    console.log(error);
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.getLoadShipping = async (req, res) => {
  try {
    const user = req.user;
    const id = req.params.id;
    if (user.role.toLowerCase() !== 'shipper') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'available only for Shipper'});
      });
      return res.status(400).send({message: 'available only for Shipper'});
    }
    const load = await Loads.findOne({_id: id});
    if (!load) {
      morgan.token('body', (req) => JSON.stringify({message: 'Not found'}));
      return res.status(400).send({message: 'Not found'});
    }
    const truck = await Trucks.findOne({assigned_to: load.assigned_to});
    if (!truck) {
      morgan.token('body', (req) => JSON.stringify({message: 'Not found'}));
      return res.status(400).send({message: 'Not found'});
    }
    morgan.token('body', (req) => JSON.stringify({load, truck}));
    return res.status(200).send({load, truck});
  } catch (error) {
    console.log(error);
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
// kyrylo
