const User = require('../models/user');
const bcrypt = require('bcrypt');
const rounds = 10;
const utils = require('../utils/utils');
const morgan = require('morgan');

exports.login = async (req, res) => {
  const {email, password} = req.body;
  try {
    const user = await User.findOne({email});
    if (!user) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Invalid email/password'});
      });
      return res.status(400).send({message: 'Invalid email/password'});
    }
    if (await bcrypt.compare(password, user.password)) {
      const token = utils.generateToken(user);

      morgan.token('body', (req) => {
        return JSON.stringify({jwt_token: token});
      });
      return res.status(200).send({jwt_token: token});
    }

    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Invalid email/password'});
    });
    return res.status(400).send({message: 'Invalid email/password'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};

exports.register = async (req, res) => {
  const {email, password: plainTextPassword, role} = req.body;

  if (!email || typeof email !== 'string') {
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Invalid email'});
    });
    return res.status(400).send({message: 'Invalid email'});
  }

  if (!plainTextPassword || typeof plainTextPassword !== 'string') {
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Invalid password'});
    });
    return res.status(400).send({message: 'Invalid password'});
  }

  if (plainTextPassword.length < 5) {
    morgan.token('body', (req) => {
      return JSON.stringify({
        message: 'Password too small. Should be atleast 6 characters',
      });
    });
    return res.status(400).send({
      message: 'Password too small. Should be atleast 6 characters',
    });
  }

  const password = await bcrypt.hash(plainTextPassword, rounds);
  const createdDate = new Date().toJSON();
  try {
    await User.create({
      email,
      created_date: createdDate,
      password,
      role,
    });
  } catch (error) {
    if (error.code === 11000) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'email already in use'});
      });
      return res.status(400).send({message: 'email already in use'});
    }
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }

  morgan.token('body', (req) => JSON.stringify({
    message: 'Profile created successfully',
  }));
  return res.status(200).send({message: 'Profile created successfully'});
};

exports.forgot_password = async (req, res) => {
  const {email} = req.body;
  try {
    const user = await User.findOne({email});
    if (!user) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: `No user with ${email}`});
      });
      return res.status(400).send({message: `No user with ${email}`});
    }
    morgan.token('body', (req) => {
      return JSON.stringify({
        message: `New password sent to your email address`,
      });
    });
    return res.status(200).send({
      message: 'New password sent to your email address',
    });
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
