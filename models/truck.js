const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const model = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
}, {collection: 'trucks', versionKey: false});

// eslint-disable-next-line new-cap
module.exports = new mongoose.model('Truck', model);
