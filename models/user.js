const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const model = mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
}, {collection: 'users', versionKey: false});

// eslint-disable-next-line new-cap
module.exports = new mongoose.model('User', model);
