const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const model = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [{
    message: {
      type: String,
      required: false,
    },
    time: {
      type: String,
      required: false,
    },
    _id: false,
  }],
  created_date: {
    type: String,
    required: true,
  },
}, {collection: 'loads', versionKey: false});

// eslint-disable-next-line new-cap
module.exports = new mongoose.model('Load', model);
